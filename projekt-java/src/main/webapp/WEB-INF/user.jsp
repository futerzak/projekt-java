<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
    <jsp:attribute name="title">User</jsp:attribute>
    <jsp:body>
        <h2>Hello ${pageContext.request.userPrincipal.name}!</h2>
        <a href="${pageContext.request.contextPath}/logout">Logout</a>
    </jsp:body>
</t:main>

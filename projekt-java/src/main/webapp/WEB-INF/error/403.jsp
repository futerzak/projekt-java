<%--
  Created by IntelliJ IDEA.
  User: lukasz
  Date: 17.02.15
  Time: 00:54
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />

<t:main>
  <jsp:attribute name="title">Błąd roli!</jsp:attribute>
  <jsp:attribute name="nav">403</jsp:attribute>
  <jsp:body>
    <div class="jumbotron" align="center">

      <h2><b>Błąd!</b></h2>
    </div>
    <div align="center">
      <h3>Błąd uprawnień! Zaloguj się na konto z odpowiednimi uprawnieniami!</h3>
    </div>
    </div>
  </jsp:body>
</t:main>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />

<t:main>
    <jsp:attribute name="title">Zapisy na studia</jsp:attribute>
    <jsp:attribute name="nav">index</jsp:attribute>
    <jsp:body>
        <div class="jumbotron">
            <h1>Witaj w zapisach!</h1>
        </div>
    </jsp:body>
</t:main>


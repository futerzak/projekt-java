<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text" var="Text"/>

<t:main>
    <jsp:attribute name="title">Zapis na stanowisko</jsp:attribute>
    <jsp:attribute name="nav">student/positionsubscribe</jsp:attribute>
    <jsp:body>
        <div class="jumbotron">
            <h1>Zapis na stanowisko</h1>
        </div>
        <form method="post" action="coursesubscribe.jsp">
            <div class="form-group">
                <label for="positions">Stanowisko</label>
                <select class="form-control" id="positions" name="position">
                    <option value>wybierz</option>
                    <c:forEach var="position" items="${positions}">
                        <option value=
                                <c:out value="${position}"/>><c:out value="${position}"/></option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="terms">Termin</label>
                <select class="form-control" id="terms" name="term">
                    <option value>wybierz</option>
                    <c:forEach var="course" begin="0" end="9"> // TODO wstawione przykładowae dane
                        <option value=
                                <c:out value="${course}"/>>Przykładowy Termin <c:out value="${course}"/></option>
                    </c:forEach>
                </select>

            </div>
            <div class="form-group">
                <input class="btn btn-default" type="submit" value="Zapisz"/>
            </div>
        </form>
    </jsp:body>
</t:main>
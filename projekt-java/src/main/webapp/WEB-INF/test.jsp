<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
    <jsp:attribute name="title">Test</jsp:attribute>
    <jsp:body>
        <h1>${testMessage}</h1>
    </jsp:body>
</t:main>

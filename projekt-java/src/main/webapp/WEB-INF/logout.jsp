<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />
<t:main>
    <jsp:attribute name="title">Logout</jsp:attribute>
    <jsp:attribute name="nav">logout</jsp:attribute>
    <jsp:body>
        <div>
            <meta HTTP-EQUIV="REFRESH" content="2; url=${pageContext.request.contextPath}/" />
            <p><fmt:message key="logout.tired" bundle="${Text}"/> <a href="${pageContext.request.contextPath}/"><fmt:message key="clickHere" bundle="${Text}"/></a></p>
        </div>
    </jsp:body>
</t:main>

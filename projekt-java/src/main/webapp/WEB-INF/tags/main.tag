<%@ tag description="Main template" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="title" required="true"%>
<%@ attribute name="nav" required="false"%>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ovh.pzmi.signcourse.i18n.text" />
<html lang="${language}">
<head>
    <link rel="shortcut icon" href="${request.contextPath}/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="${request.contextPath}/images/favicon.ico" type="image/x-icon">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap/3.3.2/css/bootstrap.min.css'>
    <script type="text/javascript"  src="${pageContext.request.contextPath}/webjars/jquery/2.1.3/jquery.js"></script>
    <script type="text/javascript"  src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/stylesheets/main.css'>
    <%--<link href="<c:url value="/stylesheets/main.css"/>" rel="stylesheet" type="text/css" />--%>
    <title>${title}</title>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}">Zapisy na studia</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class=<c:if test="${nav == 'index'}">"active"</c:if>><a href=${pageContext.request.contextPath}/>Strona główna</a></li>
                <c:if test="${empty pageContext.request.userPrincipal}">
                    <li class=<c:if test="${nav == 'register'}">"active"</c:if>><a href=${pageContext.request.contextPath}/register>Rejestracja</a></li>
                </c:if>
                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <li class=<c:if test="${nav == 'admin/users'}">"active"</c:if>><a href=${pageContext.request.contextPath}/admin/users>Użytkownicy</a></li>
                    <li class=<c:if test="${nav == 'admin/courses'}">"active"</c:if>><a href=${pageContext.request.contextPath}/admin/courses>Kursy</a></li>
                </c:if>
                <c:if test="${pageContext.request.isUserInRole('lecturer')}">
                    <li class=<c:if test="${nav == 'lecturer/courses'}">"active"</c:if>><a href=${pageContext.request.contextPath}/lecturer/courses>Kursy</a></li>
                </c:if>
                <c:if test="${pageContext.request.isUserInRole('student')}">
                    <li class=<c:if test="${nav == 'student/mysubscribes'}">"active"</c:if>><a href=${pageContext.request.contextPath}/student/terms>Moje zapisy</a></li>
                    <li class=<c:if test="${nav == 'student/coursesubscribe'}">"active"</c:if>><a href=${pageContext.request.contextPath}/student/coursesubscribe>Zapis na zajęcia</a></li>
                </c:if>
                <li class=<c:if test="${nav == 'about'}">"active"</c:if>><a href=${pageContext.request.contextPath}/about>O nas</a></li>
                <li class=<c:if test="${nav == 'contact'}">"active"</c:if>><a href=${pageContext.request.contextPath}/contact>Kontakt</a></li>


            </ul>
            <ul>
                <li id="navbar-logon" class="nav navbar-nav navbar-right">
                    <c:if test="${empty pageContext.request.remoteUser}">
                        <a class="btn btn-default" href="${pageContext.request.contextPath}/logon" role="button">Logon</a>
                    </c:if>
                    <c:if test="${not empty pageContext.request.remoteUser}">
                        <a class="btn btn-default" href="${pageContext.request.contextPath}/logout" role="button">Logout</a>
                    </c:if>

                </li>
            </ul>
        </div><!--/.nav-collapse -->

    </div>
</nav>

<div class="container">
    <c:if test="${not empty errorMessage}">
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">
                            Close</span></button>
            ${errorMessage}
        </div>
    </c:if>
    <c:if test="${not empty successMessage}">
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">
                        Close</span></button>
            ${successMessage}
        </div>
    </c:if>

        <jsp:doBody/>

    <hr>

    <footer>
        <p>&copy;
            Czernik; Pawlikowski; Topór-Futer; Żmigrodzki 2015</p>
    </footer>
</div><!-- /.container -->
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text" var="Text"/>

<t:main>
    <jsp:attribute name="title">O nas</jsp:attribute>
    <jsp:attribute name="nav">about</jsp:attribute>
    <jsp:body>
        <div class="jumbotron" align="justify">

            <h2>Informacje o projekcie</h2>
        </div>
        <div>
            <h3>System zapisów na zajęcia na studiach</h3>
            <br><h4>Witamy w systemie zapisów na studia! </h4>
            <br>Dzięki temu systemowi już więcej nie będziesz mieć problemów ze znalezieniem dla siebie miejsca na
            studiach. <br>
            Nigdy więcej kłótni o zabrane miejsca na laboratoriach, dość walki o stanowiska na lubianą przez wszystkich
            Alterę!<br>
            <br>Chcesz zapisać się na zajęcia w konkretnym dniu o konkretnej dacie i godzinie? <b>Proszę bardzo!</b><br>
            Dzięki naszemu systemowi łatwo
            zorganizujesz swój rok!<br><br>
            Jesteś Prowadzącym zajęcia i zdarza Ci się zapomnieć, z którą grupą i gdzie są zajęcia?
            <b>Żaden problem!</b><br> W naszym systemie Prowadzący mają dostęp do podglądu gdzie i kiedy rozpoczynają
            się i kończą zajęcia.
            <br><br>
            <br>Serwis powstał w ramach zaliczenia przedmiotu Programowanie w Języku Java.

        </div>
    </jsp:body>
</t:main>
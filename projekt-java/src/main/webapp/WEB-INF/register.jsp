<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />
<t:main>
    <jsp:attribute name="title"><fmt:message key="register.title" bundle="${Text}"/></jsp:attribute>
    <jsp:attribute name="nav">register</jsp:attribute>
    <jsp:body>
        <form action="register" method="post">
            <div class="form-group <c:if test='${not empty messages.emailError}'> has-error </c:if>">
                <label for="inputEmail"><fmt:message key="emailAddress" bundle="${Text}"/></label>
                <input type="email" class="form-control" name="email" id="inputEmail" value="${fn:escapeXml(param.email)}" placeholder="<fmt:message key="register.enterEmail" bundle="${Text}"/>">
                <c:if test='${not empty messages.emailError}'>
                    <div class="alert alert-danger" role="alert">${messages.emailError}</div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.passwordError}'> has-error </c:if>">
                <label for="inputPassword"><fmt:message key="Password" bundle="${Text}"/></label>
                <input type="password" class="form-control" name="password" id="inputPassword" placeholder="<fmt:message key="password" bundle="${Text}"/>">
                <c:if test='${not empty messages.passwordError}'>
                    <div class="alert alert-danger" role="alert">
                        ${messages.passwordError}
                    </div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.albumNumberError}'> has-error </c:if>">
                <label for="inputAlbumNumber"><fmt:message key="AlbumNumber" bundle="${Text}"/></label>
                <input type="number" class="form-control" name="albumNumber" id="inputAlbumNumber" value="${fn:escapeXml(param.albumNumber)}" placeholder="<fmt:message key="AlbumNumber" bundle="${Text}"/>">
                <c:if test='${not empty messages.albumNumberError}'>
                    <div class="alert alert-danger" role="alert">
                        ${messages.albumNumberError}
                    </div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.nameError}'> has-error </c:if>">
                <label for="inputName"><fmt:message key="name" bundle="${Text}"/></label>
                <input type="text" class="form-control" name="name" id="inputName" value="${fn:escapeXml(param.name)}" placeholder="<fmt:message key="register.enterName" bundle="${Text}"/>">
                <c:if test='${not empty messages.nameError}'>
                    <div class="alert alert-danger" role="alert">${messages.nameError}</div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.surnameError}'> has-error </c:if>">
                <label for="inputSurname"><fmt:message key="surname" bundle="${Text}"/></label>
                <input type="text" class="form-control" name="surname" id="inputSurname" value="${fn:escapeXml(param.surname)}" placeholder="<fmt:message key="register.enterSurname" bundle="${Text}"/>">
                <c:if test='${not empty messages.surnameError}'>
                    <div class="alert alert-danger" role="alert">${messages.surnameError}</div>
                </c:if>
            </div>
            <button type="submit" class="btn btn-default"><fmt:message key="submit" bundle="${Text}"/></button>
        </form>
    </jsp:body>
</t:main>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />
<t:main>
    <jsp:attribute name="title"><fmt:message key="admin.modifyuser" bundle="${Text}"/></jsp:attribute>
    <jsp:attribute name="nav">admin/modifyuser</jsp:attribute>
    <jsp:body>
        <form action="modifyuser" method="post">
            <input id="id" type="hidden" value="${user.id}" name="id">
            <div class="form-group <c:if test='${not empty messages.emailError}'> has-error </c:if>">
                <label for="inputEmail"><fmt:message key="emailAddress" bundle="${Text}"/></label>
                <input type="email" class="form-control" name="email" id="inputEmail" value="${fn:escapeXml(user.email)}" placeholder="<fmt:message key="register.enterEmail" bundle="${Text}"/>">
                <c:if test='${not empty messages.emailError}'>
                    <div class="alert alert-danger" role="alert">${messages.emailError}</div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.albumNumberError}'> has-error </c:if>">
                <label for="inputAlbumNumber"><fmt:message key="AlbumNumber" bundle="${Text}"/></label>
                <input type="number" class="form-control" name="albumNumber" id="inputAlbumNumber" value="${fn:escapeXml(user.albumNumber)}" placeholder="<fmt:message key="AlbumNumber" bundle="${Text}"/>">
                <c:if test='${not empty messages.albumNumberError}'>
                    <div class="alert alert-danger" role="alert">
                            ${messages.albumNumberError}
                    </div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.nameError}'> has-error </c:if>">
                <label for="inputName"><fmt:message key="name" bundle="${Text}"/></label>
                <input type="text" class="form-control" name="name" id="inputName" value="${fn:escapeXml(user.name)}" placeholder="<fmt:message key="register.enterName" bundle="${Text}"/>">
                <c:if test='${not empty messages.nameError}'>
                    <div class="alert alert-danger" role="alert">${messages.nameError}</div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.surnameError}'> has-error </c:if>">
                <label for="inputSurname"><fmt:message key="surname" bundle="${Text}"/></label>
                <input type="text" class="form-control" name="surname" id="inputSurname" value="${fn:escapeXml(user.surname)}" placeholder="<fmt:message key="register.enterSurname" bundle="${Text}"/>">
                <c:if test='${not empty messages.surnameError}'>
                    <div class="alert alert-danger" role="alert">${messages.surnameError}</div>
                </c:if>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><fmt:message key="roles" bundle="${Text}"/></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="role" value="1" <c:if test="${roles[1]}">checked</c:if>> admin
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="role" value="2" <c:if test="${roles[2]}">checked</c:if>> user
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="role" value="3" <c:if test="${roles[3]}">checked</c:if>> lecturer
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="role" value="4" <c:if test="${roles[4]}">checked</c:if>> student
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-default"><fmt:message key="submit" bundle="${Text}"/></button>
            <a class="btn btn-default" href="users" role="button"><fmt:message key="cancel" bundle="${Text}"/></a>
        </form>
    </jsp:body>
</t:main>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />
<t:main>
    <jsp:attribute name="title"><fmt:message key="admin.courses" bundle="${Text}"/></jsp:attribute>
    <jsp:attribute name="nav">admin/courses</jsp:attribute>
    <jsp:body>
        <table class="table table-hover" data-link="row">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nazwa</th>
                    <th>Czestotliwosc</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="element" items="${courses}" varStatus="status">
                <tr>
                    <td>${element.id}</td>
                    <td>${element.name}</td>
                    <td>${element.frequency}</td>
                    <td><a class="btn btn-default" href="${pageContext.request.contextPath}/admin/modifycourse?id=${element.id}" role="button"><fmt:message key="modify" bundle="${Text}"/></a></td>
                </tr>
            </c:forEach>
            </tbody>


        </table>
    </jsp:body>
</t:main>

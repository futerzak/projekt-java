<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />
<t:main>
    <jsp:attribute name="title"><fmt:message key="admin.users" bundle="${Text}"/></jsp:attribute>
    <jsp:attribute name="nav">admin/users</jsp:attribute>
    <jsp:body>
        
        <script type="text/javascript"  src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.js"></script>
        <link rel='stylesheet' href='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>
        <%--<link rel='stylesheet' href='//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css'>--%>


        <script>
            $(document).ready(function () {
                $('#users').DataTable();
            });
        </script>
        
        
        <table id="users" class="table table-hover" data-link="row">
            <thead>
                <tr>
                    <th>Id</th>
                    <th><fmt:message key="AlbumNumber" bundle="${Text}"/></th>
                    <th><fmt:message key="name" bundle="${Text}"/></th>
                    <th><fmt:message key="surname" bundle="${Text}"/></th>
                    <th><fmt:message key="emailAddress" bundle="${Text}"/></th>
                    <th><fmt:message key="admin.users.unactivated" bundle="${Text}"/></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="element" items="${users}" varStatus="status">
                    <tr <c:if test="${notActive[element.id]}">class="info"</c:if>>
                        <td>${element.id}</td>
                        <td>${element.albumNumber}</td>
                        <td>${element.name}</td>
                        <td>${element.surname}</td>
                        <td>${element.email}</td>
                        <td>${notActive[element.id]}</td>
                        <td><a class="btn btn-default" href="${pageContext.request.contextPath}/admin/modifyuser?id=${element.id}" role="button"><fmt:message key="modify" bundle="${Text}"/></a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </jsp:body>
</t:main>

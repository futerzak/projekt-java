<%--
  Created by IntelliJ IDEA.
  User: Maciej
  Date: 2015-02-17
  Time: 01:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />
<t:main>
    <jsp:attribute name="title"><fmt:message key="lecturer.addterm" bundle="${Text}"/></jsp:attribute>
    <jsp:attribute name="nav">lecturer/addterm</jsp:attribute>
    <jsp:body>
        <form action="addterm" method="post">
            <h1>${term.description}</h1> <%--TODO do formatowania--%>
            <div class="form-group <c:if test='${not empty messages.startDateError}'> has-error </c:if>">
                <label for="inputStartDate"><fmt:message key="startdate" bundle="${Text}"/></label>
                <input type="datetime-local" class="form-control" name="startDate" id="inputStartDate">
                <c:if test='${not empty messages.startDateError}'>
                    <div class="alert alert-danger" role="alert">
                            ${messages.startDateError}
                    </div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.endDateError}'> has-error </c:if>">
                <label for="inputEndDate"><fmt:message key="enddate" bundle="${Text}"/></label>
                <input type="datetime-local" class="form-control" name="endDate" id="inputEndDate">
                <c:if test='${not empty messages.endDateError}'>
                    <div class="alert alert-danger" role="alert">
                            ${messages.endDateError}
                    </div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.sizeError}'> has-error </c:if>">
                <label for="inputSize"><fmt:message key="size" bundle="${Text}"/></label>
                <input type="number" class="form-control" name="size" id="inputSize" placeholder="<fmt:message key="lecturer.enterSize" bundle="${Text}"/>">
                <c:if test='${not empty messages.sizeError}'>
                    <div class="alert alert-danger" role="alert">${messages.sizeError}</div>
                </c:if>
            </div>
            <div class="form-group <c:if test='${not empty messages.groupError}'> has-error </c:if>">
                <label for="inputGroup"><fmt:message key="group" bundle="${Text}"/></label>
                <input type="number" class="form-control" name="group" id="inputGroup" placeholder="<fmt:message key="lecturer.enterGroup" bundle="${Text}"/>">
                <c:if test='${not empty messages.groupError}'>
                    <div class="alert alert-danger" role="alert">${messages.groupError}</div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="inputCourseId"><fmt:message key="courseId" bundle="${Text}"/></label>
                <select name="courseId" id="inputCourseId">
                    <c:forEach var="element" items="${courses}" varStatus="status">
                        <option value=${element.id}>${element.name}</option>
                    </c:forEach>
                </select>
            </div>

            <button type="submit" class="btn btn-default"><fmt:message key="submit" bundle="${Text}"/></button>
            <a class="btn btn-default" href="users" role="button"><fmt:message key="cancel" bundle="${Text}"/></a>
        </form>
    </jsp:body>
</t:main>


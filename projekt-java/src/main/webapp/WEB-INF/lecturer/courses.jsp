<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text" var="Text"/>

<t:main>
    <jsp:attribute name="title">Moje przedmioty</jsp:attribute>
    <jsp:attribute name="nav">lecturer/courses</jsp:attribute>
    <jsp:body>
        <script type="text/javascript"
                src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <script type="text/javascript" charset="utf8"
                src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.js"></script>
        <link rel='stylesheet'
              href='//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>

        <script>
            $(document).ready(function () {
                $('#terms').DataTable();
            });
        </script>

        <div>
            <table id="courses" class="table table-hover" data-link="row">
                <thead>
                <tr>
                    <th><fmt:message key="subject" bundle="${Text}"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="element" items="${courses}" varStatus="status">
                    <tr>
                        <td><a href="${pageContext.request.contextPath}/lecturer/terms?id=${element.id}">${element.name}</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </jsp:body>
</t:main>
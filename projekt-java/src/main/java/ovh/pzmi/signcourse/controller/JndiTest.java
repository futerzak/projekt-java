package ovh.pzmi.signcourse.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by dexior on 30.01.15.
 */
@WebServlet(name = "JndiTest",
        urlPatterns = {"/test"})
public class JndiTest extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(JndiTest.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");

        } catch (NamingException e) {
            e.printStackTrace();
        }
        if (dataSource == null) {
            logger.error("Datasource is null.");
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
            return;
        }

        try (Connection con = dataSource.getConnection()){
            String sql = "select username from users limit 1";
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            String user;
            if (rs.next()) {
                user = rs.getString("username");
                request.setAttribute("testMessage", user);
            }
            statement.close();
            rs.close();
        } catch (SQLException e) {
            logger.warn("SQL fail", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/test.jsp");
        requestDispatcher.include(request, response);

    }
}

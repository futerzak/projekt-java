package ovh.pzmi.signcourse.controller.student;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Course;
import ovh.pzmi.signcourse.model.Term;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.model.UserTerm;
import ovh.pzmi.signcourse.service.CourseService;
import ovh.pzmi.signcourse.service.TermService;
import ovh.pzmi.signcourse.service.UserService;
import ovh.pzmi.signcourse.service.UserTermService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

/**
 * Created by futer_000 on 2015-02-16.
 */
@WebServlet(name = "CourseSubscribeController",
        urlPatterns = {"/student/coursesubscribe"})
public class CourseSubscribeController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(CourseSubscribeController.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Long term = Long.parseLong(request.getParameter("term"));

        DataSource dataSource = null;
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }

        Principal userPrincipal = request.getUserPrincipal();
        UserService userService = new UserService(dataSource);
        User user = userService.find(userPrincipal.getName());

        UserTerm userTerm = new UserTerm();
        userTerm.setTerm_id(term);
        userTerm.setUser_id(user.getId());


        UserTermService userTermService = new UserTermService(dataSource);
        userTermService.create(userTerm);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/student/coursesubscribe.jsp");
        request.setAttribute("successMessage","Pomyślnie zapisano na termin");
        requestDispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        CourseService courseService = new CourseService(dataSource);
        List<Course> courses = courseService.list();
        request.setAttribute("courses", courses);

        TermService termService = new TermService(dataSource);
        List<Term> terms = termService.list();
        request.setAttribute("terms", terms);


        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/student/coursesubscribe.jsp");
        requestDispatcher.include(request, response);
    }

}

package ovh.pzmi.signcourse.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.model.UserRole;
import ovh.pzmi.signcourse.service.UserRoleService;
import ovh.pzmi.signcourse.service.UserService;
import ovh.pzmi.signcourse.util.AccountUtils;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by dexior on 06.02.15.
 */
@WebServlet(name = "RegisterController",
            urlPatterns = {"/register"})
public class RegisterController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(RegisterController.class);
    final static int MIN_PASSWORD_LEN = 6;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        Map<String, String> messages = new HashMap<>();
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/register.jsp");
        String email = request.getParameter("email");
        String password =  request.getParameter("password");
        String albumNumberString = request.getParameter("albumNumber");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        int albumNumber = Integer.parseInt(albumNumberString);
        UserService userService = new UserService(dataSource);
        User user = userService.find(email, albumNumber);
        if (user != null) {
            if (user.getAlbumNumber() == albumNumber) {
                messages.put("albumNumberError", resourceBundle.getString("register.error.albumNumber"));
            }
            if (user.getEmail() == email) {
                messages.put("emailError", resourceBundle.getString("register.error.email"));
            }
        } else {
            if (!validatePassword(password, MIN_PASSWORD_LEN)) {
                messages.put("passwordError", getPasswordError(resourceBundle, MIN_PASSWORD_LEN));
            } else {
                user = new User();
                user.setAlbumNumber(albumNumber);
                user.setEmail(email);
                user.setPassword(AccountUtils.createPassword(email, password));
                user.setName(name);
                user.setSurname(surname);
//                user.setId(null);
                userService.create(user);
                UserRole userRole = new UserRole();
                userRole.setRole_id(2L);
                userRole.setUser_id(user.getId());
                UserRoleService userRoleService = new UserRoleService(dataSource);
                userRoleService.create(userRole);

                AccountUtils.sendMailRegistered(request, email);
                request.setAttribute("successMessage", resourceBundle.getString("register.success"));
            }
        }
        
        if (!messages.isEmpty()) {
            request.setAttribute("errorMessage", resourceBundle.getString("register.checkFields"));
        }
        request.setAttribute("messages", messages);
        requestDispatcher.include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/register.jsp");
        requestDispatcher.forward(request, response);
    }
    
    private Boolean validatePassword(String password, int minLength) {
        if (password.length() < minLength) {
            return false;
        }
        return true;
    }

    private String getPasswordError(ResourceBundle resourceBundle, Object... args) {
        MessageFormat formatter = new MessageFormat("{0}");
        formatter.applyPattern(resourceBundle.getString("register.error.password.length"));

        String output = formatter.format(args);
        return output;

    }
}

package ovh.pzmi.signcourse.controller;

import org.eclipse.jetty.util.security.Password;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by dexior on 31.01.15.
 */
@WebServlet(name = "PasswordTest",
            urlPatterns = {"/password"})
public class PasswordTest extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String p = Password.Crypt.crypt("zapisystudia@gmail.com", "admin");
        System.out.println(p);
    }
}

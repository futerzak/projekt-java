package ovh.pzmi.signcourse.controller.lecturer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Course;
import ovh.pzmi.signcourse.model.Term;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.service.CourseService;
import ovh.pzmi.signcourse.service.TermService;
import ovh.pzmi.signcourse.service.UserService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Maciej on 2015-02-17.
 */

@WebServlet(name = "LecturerAddTermController",
        urlPatterns = {"lecturer/addterm"})
public class AddTermController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(AddTermController.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }

        TermService termService = new TermService(dataSource);
        String sDate = request.getParameter("startDate").replace("T", " ") + ":00";
        String eDate = request.getParameter("endDate").replace("T", " ") + ":00";
        Timestamp startDate = Timestamp.valueOf(sDate);
        Timestamp endDate = Timestamp.valueOf(eDate);
        int size = Integer.parseInt(request.getParameter("size"));
        int group = Integer.parseInt(request.getParameter("group"));
        Long courseId = Long.parseLong(request.getParameter("courseId"));

        CourseService courseService = new CourseService(dataSource);
        String name = courseService.find(courseId).getName();

        Term term = new Term();

        term.setDescription(name);
        term.setStart_date(startDate);
        term.setEnd_date(endDate);
        term.setCapacity(0);
        term.setSize(size);
        term.setGroup(group);
        term.setCourse_id(courseId);
        termService.create(term);

        request.setAttribute("successMessage", "Temin utworzono pomyślnie");

        Principal userPrincipal = request.getUserPrincipal();
        UserService userService = new UserService(dataSource);
        User user = userService.find(userPrincipal.getName());
        List<Course> courses = courseService.listByUserID(user.getId());

        request.setAttribute("courses", courses);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/lecturer/courses.jsp");
        requestDispatcher.include(request, response);


    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }

        Principal userPrincipal = request.getUserPrincipal();
        UserService userService = new UserService(dataSource);
        User user = userService.find(userPrincipal.getName());
        CourseService courseService = new CourseService(dataSource);
        List<Course> courses = courseService.listByUserID(user.getId());

        request.setAttribute("courses", courses);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/lecturer/addTerm.jsp");
        requestDispatcher.include(request, response);


    }

}

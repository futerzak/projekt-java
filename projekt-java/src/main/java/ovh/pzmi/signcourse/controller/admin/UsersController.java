package ovh.pzmi.signcourse.controller.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Role;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.model.UserRole;
import ovh.pzmi.signcourse.service.RoleService;
import ovh.pzmi.signcourse.service.UserRoleService;
import ovh.pzmi.signcourse.service.UserService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dexior on 11.02.15.
 */
@WebServlet(name = "Users",
            urlPatterns = {"/admin/users"})
public class UsersController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(UsersController.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        UserService userService = new UserService(dataSource);
        List<User> users = userService.list();
        UserRoleService userRoleService = new UserRoleService(dataSource);
        HashMap<Long, Boolean> notActive = new HashMap<>();
        for (User user : users) {
            Long userId = user.getId();
            notActive.put(userId, userRoleService.isSimpleUser(userId));
        }
        request.setAttribute("users", users);
        request.setAttribute("notActive", notActive);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/admin/users.jsp");
        requestDispatcher.include(request, response);
    }
}

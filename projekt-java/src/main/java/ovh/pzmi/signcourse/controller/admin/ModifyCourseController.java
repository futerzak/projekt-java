package ovh.pzmi.signcourse.controller.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Course;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.service.CourseService;
import ovh.pzmi.signcourse.service.UserService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Maciej on 2015-02-16.
 */
@WebServlet(name = "AdminModifyCoursesController",
        urlPatterns = {"admin/modifycourse"})
public class ModifyCourseController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(ModifyCourseController.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }

        Long courseId = Long.parseLong(request.getParameter("id"));

        CourseService courseService = new CourseService(dataSource);
        Course course = courseService.find(courseId);

        String name = request.getParameter("courseName");
        int intfreq = Integer.parseInt(request.getParameter("courseFrequency"));
        Course.Frequency frequency = course.getFrequency();
        switch (intfreq){
            case 1:{
                frequency = Course.Frequency.tydzien;
                break;
            }
            case 2:{
                frequency = Course.Frequency.tygodnie;
                break;
            }
            case 3:{
                frequency = Course.Frequency.miesiac;
                break;
            }
        }
        Long lecturerId = Long.parseLong(request.getParameter("lecturerId"));

        course.setName(name);
        course.setFrequency(frequency);
        course.setUser_id(lecturerId);
        courseService.update(course);

        List<Course> courses = courseService.list();

        request.setAttribute("courses", courses);
        request.setAttribute("successMessage", "Zmodyfikowano pomyślnie");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/admin/courses.jsp");
        requestDispatcher.forward(request, response);

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        CourseService courseService = new CourseService(dataSource);
        Long courseId = Long.parseLong(request.getParameter("id"));
        Course course = courseService.find(courseId);

        if (course == null) {
            request.setAttribute("errorMessage", getCourseError(resourceBundle, courseId));
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin/courses");
            requestDispatcher.forward(request, response);
            return;
        }

        UserService userService = new UserService(dataSource);
        List<User> lecturers = userService.listLecturers();

        request.setAttribute("lecturers", lecturers);
        request.setAttribute("course", course);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/admin/modifyCourse.jsp");
        requestDispatcher.include(request, response);

    }

    public String getCourseError(ResourceBundle resourceBundle, Object... args){
        MessageFormat formatter = new MessageFormat("{0}");
        formatter.applyPattern(resourceBundle.getString("admin.modifycourse.nosuchcourse"));

        String output = formatter.format(args);
        return output;

    }
}

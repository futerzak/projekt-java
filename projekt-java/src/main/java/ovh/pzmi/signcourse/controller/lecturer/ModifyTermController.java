package ovh.pzmi.signcourse.controller.lecturer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Course;
import ovh.pzmi.signcourse.model.Term;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.service.CourseService;
import ovh.pzmi.signcourse.service.TermService;
import ovh.pzmi.signcourse.service.UserService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.security.Principal;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Maciej on 2015-02-17.
 */
@WebServlet(name = "LecturerModifyTermController",
        urlPatterns = {"lecturer/modifyterm"})
public class ModifyTermController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(ModifyTermController.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        Long termId = Long.parseLong(request.getParameter("id"));
        TermService termService = new TermService(dataSource);
        Term term = termService.find(termId);
        String sDate = request.getParameter("startDate").replace("T", " ") + ":00";
        String eDate = request.getParameter("endDate").replace("T", " ") + ":00";
        Timestamp startDate = Timestamp.valueOf(sDate);
        Timestamp endDate = Timestamp.valueOf(eDate);
        int size = Integer.parseInt(request.getParameter("size"));
        int group = Integer.parseInt(request.getParameter("group"));
        Long courseId = Long.parseLong(request.getParameter("courseId"));

        term.setStart_date(startDate);
        term.setEnd_date(endDate);
        term.setSize(size);
        term.setGroup(group);
        term.setCourse_id(courseId);
        termService.update(term);

        request.setAttribute("successMessage", "Zmodyfikowano pomyślnie");

        List<Term> terms = termService.listByCourseId(courseId);

        Calendar c = Calendar.getInstance();
        int dayOfWeek;
        long startTime = 0, endTime = 0;
        HashMap<Long, String> daysOfWeek = new HashMap<>();
        HashMap<Long, String> startHours = new HashMap<>();
        HashMap<Long, String> endHours = new HashMap<>();
        SimpleDateFormat hour = new SimpleDateFormat ("HH:mm");

        String[] days =
                {"Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"};
        for(Term x : terms) {
            c.setTime(x.getStart_date());
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            termId = x.getId();
            startTime = x.getStart_date().getTime();
            endTime = x.getEnd_date().getTime();
            daysOfWeek.put(termId, days[dayOfWeek - 1]);
            startHours.put(termId, hour.format(startTime));
            endHours.put(termId, hour.format(endTime));
        }

        request.setAttribute("daysOfWeek", daysOfWeek);
        request.setAttribute("startHours", startHours);
        request.setAttribute("endHours", endHours);
        request.setAttribute("terms", terms);


        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/lecturer/terms.jsp");
        requestDispatcher.forward(request, response);
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        TermService termService = new TermService(dataSource);
        Long termId = Long.parseLong(request.getParameter("id"));
        Term term = termService.find(termId);

        if (term == null) {
            request.setAttribute("errorMessage", getTermError(resourceBundle, termId));
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/lecturer/terms");
            requestDispatcher.forward(request, response);
            return;
        }
        Principal userPrincipal = request.getUserPrincipal();
        UserService userService = new UserService(dataSource);
        User user = userService.find(userPrincipal.getName());

        CourseService courseService = new CourseService(dataSource);
        List<Course> courses = courseService.listByUserID(user.getId());

        request.setAttribute("courses", courses);
        request.setAttribute("term", term);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/lecturer/modifyTerm.jsp");
        requestDispatcher.include(request, response);

    }

    public String getTermError(ResourceBundle resourceBundle, Object... args){
        MessageFormat formatter = new MessageFormat("{0}");
        formatter.applyPattern(resourceBundle.getString("lecturer.modifyterm.nosuchterm"));

        String output = formatter.format(args);
        return output;

    }
}

package ovh.pzmi.signcourse.controller.student;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Term;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.service.TermService;
import ovh.pzmi.signcourse.service.UserService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by bystrzak on 14.02.15.
 */
@WebServlet(name = "StudentTermsController",
        urlPatterns = {"/student/terms"})
public class TermsController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(TermsController.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        TermService termService = new TermService(dataSource);
        Principal userPrincipal = request.getUserPrincipal();
        UserService userService = new UserService(dataSource);
        User user = userService.find(userPrincipal.getName());


        List<Term> terms = termService.findByUserId(user.getId());

        Calendar c = Calendar.getInstance();
        int dayOfWeek;
        long startTime = 0, endTime = 0;
        Long termId = 0L;
        HashMap<Long, String> daysOfWeek = new HashMap<>();
        HashMap<Long, String> startHours = new HashMap<>();
        HashMap<Long, String> endHours = new HashMap<>();
        SimpleDateFormat hour = new SimpleDateFormat ("HH:mm");

        String[] days =
                {"Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"};
        for(Term x : terms) {
            c.setTime(x.getStart_date());
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            termId = x.getId();
            startTime = x.getStart_date().getTime();
            endTime = x.getEnd_date().getTime();
            daysOfWeek.put(termId, days[dayOfWeek - 1]);
            startHours.put(termId, hour.format(startTime));
            endHours.put(termId, hour.format(endTime));
        }

        request.setAttribute("daysOfWeek", daysOfWeek);
        request.setAttribute("startHours", startHours);
        request.setAttribute("endHours", endHours);
        request.setAttribute("terms", terms);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/student/terms.jsp");
        requestDispatcher.include(request, response);
    }
}

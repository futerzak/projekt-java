package ovh.pzmi.signcourse.model;

/**
 * Created by Maciej on 2015-01-28.
 */
public class UserTerm {

    private Long user_id;

    private Long term_id;

    private boolean isReserve;


    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getTerm_id() {
        return term_id;
    }

    public void setTerm_id(Long term_id) {
        this.term_id = term_id;
    }

    public boolean isReserve() {
        return isReserve;
    }

    public void setReserve(boolean isReserve) {
        this.isReserve = isReserve;
    }
}

package ovh.pzmi.signcourse.model;

/**
 * Created by Maciej on 2015-01-28.
 */
public class Course {

    public enum Frequency {
        tydzien, tygodnie, miesiac
    }

    private Long id;

    private String name;

    private Frequency frequency;

    private Long user_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }
}

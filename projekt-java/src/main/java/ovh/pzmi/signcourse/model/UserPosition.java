package ovh.pzmi.signcourse.model;

import java.util.Date;

/**
 * Created by Maciej on 2015-01-28.
 */
public class UserPosition {

    private Long user_id;

    private Long position_id;

    private Date date;

    private boolean isReserve;

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getPosition_id() {
        return position_id;
    }

    public void setPosition_id(Long position_id) {
        this.position_id = position_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isReserve() {
        return isReserve;
    }

    public void setReserve(boolean isReserve) {
        this.isReserve = isReserve;
    }
}

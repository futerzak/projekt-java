package ovh.pzmi.signcourse.service;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.UserRole;
import ovh.pzmi.signcourse.util.AccountUtils;
import ovh.pzmi.signcourse.util.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dexior on 05.02.15.
 */
public class UserRoleService {
    final private Logger logger = LoggerFactory.getLogger(UserRoleService.class);
    public static final String SQL_FIND_BY_ID = "select * from user_roles where role_id = ? and user_id = ?";
    public static final String SQL_FIND_BY_USER = "select * from user_roles where user_id = ?";
    public static final String SQL_LIST_ORDER_BY_ID = "select * from user_roles order by role_id, user_id";
    public static final String SQL_INSERT = "insert ignore into user_roles(role_id, user_id) values(?, ?)";
    public static final String SQL_DELETE_BY_ROLE_ID_USER_ID = "delete from user_roles where role_id = ? and user_id = ?";
    public static final String SQL_COUNT_ROLES = "select count(1) as count from user_roles where user_id = ? group by user_id";
    private DataSource dataSource;

    public UserRoleService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public UserRole find(String sql, Object... values) {
        UserRole userRole = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                userRole = map(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserRole from database.", e);
        }
        return userRole;
    }

    public UserRole find(long role_id, long user_id) {
        return find(SQL_FIND_BY_ID, role_id, user_id);
    }

    public List<UserRole> list() {
        List<UserRole> userRoles = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                userRoles.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserRole from database.", e);
        }

        return userRoles;
    }

    public List<UserRole> list(long user_id) {
        List<UserRole> userRoles = new ArrayList<>();
        
        Object[] values = {
                user_id
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_FIND_BY_USER, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                userRoles.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserRole from database.", e);
        }

        return userRoles;
    }

    public void create(UserRole userRole) {
        Boolean created = false;
        if (userRole.getUser_id() != null || userRole.getRole_id() != null) {
            created = true;
        }
        Object[] values = {
            userRole.getRole_id(),
                userRole.getUser_id()

        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_INSERT, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0 && !created) {
                logger.warn("Creating userRole failed, no rows affected.");
            }

        } catch (SQLException e) {
            logger.error("Couldn't insert userRole");
        }
    }
    
    public void delete(UserRole userRole) {
        Object[] values = {
            userRole.getRole_id(),
                userRole.getUser_id()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_DELETE_BY_ROLE_ID_USER_ID, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Deleting userRole failed, no rows affected.");
            } else {
                userRole.setUser_id(null);
                userRole.setRole_id(null);
            }
        } catch (SQLException e) {
            logger.error("Deleting userRole failed.", e);
        }
    }
    
    public Boolean isSimpleUser(Long userId) {
        Boolean isSimpleUser = null;
        
        Object[] values = {
                userId
        };
        
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_COUNT_ROLES, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                int count = resultSet.getInt("count");
                isSimpleUser = count > 1 ? false : true;
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserRole from database.", e);
        }
        return isSimpleUser;
        
    }

    private UserRole map(ResultSet resultSet) throws SQLException {
        UserRole userRole = new UserRole();
        userRole.setRole_id(resultSet.getLong("role_id"));
        userRole.setUser_id(resultSet.getLong("user_id"));
        return userRole;
    }
}
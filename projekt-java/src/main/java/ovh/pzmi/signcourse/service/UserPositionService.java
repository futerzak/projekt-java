package ovh.pzmi.signcourse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.UserPosition;
import ovh.pzmi.signcourse.util.AccountUtils;
import ovh.pzmi.signcourse.util.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dexior on 05.02.15.
 */
public class UserPositionService {
    final private Logger logger = LoggerFactory.getLogger(UserPositionService.class);
    public static final String SQL_FIND_BY_ID = "select * from users_positions where user_id = ? and positions_id = ?";
    public static final String SQL_LIST_ORDER_BY_ID = "select * from users_positions order by user_id, positions_id";
    public static final String SQL_UPDATE = "update users_positions set date = ?, is_reserved = ? where user_id = ? and positions_id = ?";
    public static final String SQL_INSERT = "insert ignore into users_positions(user_id, positions_id, date, is_reserved) values(?, ?, ?, ?)";
    public static final String SQL_DELETE_BY_USER_ID_POSITION_ID = "delete from users_positions where user_id = ? and positions_id = ?";
    private DataSource dataSource;

    public UserPositionService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public UserPosition find(String sql, Object... values) {
        UserPosition userPosition = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                userPosition = map(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserPosition from database.", e);
        }
        return userPosition;
    }

    public UserPosition find(long id) {
        return find(SQL_FIND_BY_ID, id);
    }

    public List<UserPosition> list() {
        List<UserPosition> userPositions = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                userPositions.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserPosition from database.", e);
        }

        return userPositions;
    }

    public void create(UserPosition userPosition) {


        Object[] values = {
            userPosition.getUser_id(),
                userPosition.getPosition_id(),
                userPosition.getDate(),
                userPosition.isReserve()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_INSERT, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Creating userPosition failed, no rows affected.");
            }

        } catch (SQLException e) {
            logger.error("Couldn't insert userPosition");
        }
    }

    public void update(UserPosition userPosition) {
        if (userPosition.getUser_id() == null || userPosition.getPosition_id() == null) {
            throw new IllegalArgumentException("UserPosition is not created yet, the userPosition ID is null.");
        }

        Object[] values = {
            userPosition.getDate(),
                userPosition.isReserve(),
                userPosition.getUser_id(),
                userPosition.getPosition_id()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_UPDATE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Updating userPosition failed, no rows affected.");
            }
        } catch (SQLException e) {
            logger.warn("Couldn't update UserPosition.", e);
        }
    }

    public void delete(UserPosition userPosition) {
        Object[] values = {
                userPosition.getUser_id(),
                userPosition.getPosition_id()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_DELETE_BY_USER_ID_POSITION_ID, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Deleting userPosition failed, no rows affected.");
            } else {
                userPosition.setUser_id(null);
                userPosition.setPosition_id(null);
            }
        } catch (SQLException e) {
            logger.error("Deleting userPosition failed.", e);
        }
    }

    private UserPosition map(ResultSet resultSet) throws SQLException {
        UserPosition userPosition = new UserPosition();
        userPosition.setUser_id(resultSet.getLong("user_id"));
        userPosition.setPosition_id(resultSet.getLong("positions_id"));
        userPosition.setDate(resultSet.getDate("date"));
        userPosition.setReserve(resultSet.getBoolean("is_reserved"));
        return userPosition;
    }
}
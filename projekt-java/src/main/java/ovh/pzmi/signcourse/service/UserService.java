package ovh.pzmi.signcourse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.util.AccountUtils;
import ovh.pzmi.signcourse.util.DbUtils;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dexior on 31.01.15.
 */
public class UserService {
    final private Logger logger = LoggerFactory.getLogger(UserService.class);
    public static final String SQL_FIND_BY_ID = "select * from users where id = ?";
    public static final String SQL_FIND_BY_EMAIL = "select * from users where email = ?";
    public static final String SQL_FIND_BY_EMAIL_PASSWORD = "select * from users where email = ? and password = ?";
    public static final String SQL_FIND_BY_EMAIL_OR_ALBUM_NUMBER = "select * from users where email = ? or album_number = ?";
    public static final String SQL_FIND_BY_ALBUM_NUMBER = "select * from users where album_number = ?";
    public static final String SQL_LIST_ORDER_BY_ID = "select * from users order by id";
    public static final String SQL_LIST_LECTURERS = "select * from users u join user_roles ur on u.id = ur.user_id where ur.role_id = 3";
    public static final String SQL_UPDATE = "update users set album_number = ?, name = ?, surname = ?, email = ?, password = ? where  id = ?";
    public static final String SQL_INSERT = "insert into users(album_number, name, surname, email, password) values(?, ?, ?, ?, ?)";
    public static final String SQL_DELETE = "delete from users where id = ?";
    private DataSource dataSource;

    public UserService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public User find(String sql, Object... values) {
        User user = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                user = map(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve User from database.", e);
        }
        return user;
    }

    public User find(long id) {
        return find(SQL_FIND_BY_ID, id);
    }

    public User find(int albumNumber) {
        return find(SQL_FIND_BY_ALBUM_NUMBER, albumNumber);
    }

    public User find(String email) {
        return find(SQL_FIND_BY_EMAIL, email);
    }

    public User find(String email, int albumNumber) {
        return find(SQL_FIND_BY_EMAIL_OR_ALBUM_NUMBER, email, albumNumber);
    }

    public User findByEmailPassword(String email, String passowrd) {
        String cryptoPassword = AccountUtils.createPassword(email, passowrd);
        return find(SQL_FIND_BY_EMAIL_PASSWORD, email, cryptoPassword);
    }

    public List<User> list() {
        List<User> users = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                users.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve User from database.", e);
        }

        return users;
    }

    public List<User> listLecturers() {
        List<User> users = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_LECTURERS);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                users.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve User from database.", e);
        }

        return users;
    }
    
    public void create(User user) {
        if (user.getId() != 0) {
            throw new IllegalArgumentException("User is already created, the user ID is not null.");
        }

        Object[] values = {
                user.getAlbumNumber(),
                user.getName(),
                user.getSurname(),
                user.getEmail(),
                user.getPassword()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_INSERT, true, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Creating user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getLong(1));
                } else {
                    logger.warn("Creating user failed, no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            logger.error("Couldn't insert user");
        }
    }

    public void update(User user) {
        if (user.getId() == null) {
            throw new IllegalArgumentException("User is not created yet, the user ID is null.");
        }

        Object[] values = {
                user.getAlbumNumber(),
                user.getName(),
                user.getSurname(),
                user.getEmail(),
                user.getPassword(),
                user.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_UPDATE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Updating user failed, no rows affected.");
            }
        } catch (SQLException e) {
            logger.warn("Couldn't update User.", e);
        }
    }

    public void delete(User user) {
        Object[] values = {
                user.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_DELETE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Deleting user failed, no rows affected.");
            } else {
                user.setId(null);
            }
        } catch (SQLException e) {
            logger.error("Deleting user failed.", e);
        }
    }

    private User map(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong("id"));
        user.setAlbumNumber(resultSet.getInt("album_number"));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        return user;
    }
}

package ovh.pzmi.signcourse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Position;
import ovh.pzmi.signcourse.util.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dexior on 03.02.15.
 */
public class PositionService {
    final private Logger logger = LoggerFactory.getLogger(PositionService.class);
    public static final String SQL_FIND_BY_ID = "select * from positions where id = ?";
    public static final String SQL_FIND_BY_ROOM_NAME = "select * from positions where room = ? and name = ?";
    public static final String SQL_LIST_ORDER_BY_ID = "select * from positions order by id";
    public static final String SQL_DELETE = "delete from positions where id = ?";
    public static final String SQL_UPDATE = "update positions set name = ?, room = ?, description = ? where id = ?";
    public static final String SQL_INSERT = "insert into positions(name, room, description) values(?, ?, ?)";

    private DataSource dataSource;

    public PositionService(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public Position find(String sql, Object... values) {
        Position position = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                position = map(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Position from database.", e);
        }
        return position;
    }

    public Position find(long id) {
        return find(SQL_FIND_BY_ID, id);
    }

    public Position find(String room, String name) {
        return find(SQL_FIND_BY_ROOM_NAME, room, name);
    }

    public List<Position> list() {
        List<Position> positions = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                positions.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve User from database.", e);
        }

        return positions;
    }


    public void create(Position position) {
        if (position.getId() != null) {
            throw new IllegalArgumentException("Position is already created, the position ID is not null.");
        }

        Object[] values = {
                position.getName(),
                position.getRoom(),
                position.getDescription()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_INSERT, true, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Creating position failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    position.setId(generatedKeys.getLong(1));
                } else {
                    logger.warn("Creating position failed, no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            logger.error("Couldn't insert position");
        }
    }

    public void update(Position position) {
        if (position.getId() == null) {
            throw new IllegalArgumentException("User is not created yet, the user ID is null.");
        }

        Object[] values = {
                position.getName(),
                position.getName(),
                position.getRoom(),
                position.getDescription(),
                position.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_UPDATE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Updating position failed, no rows affected.");
            }
        } catch (SQLException e) {
            logger.warn("Couldn't update position.", e);
        }
    }

    public void delete(Position position) {
        Object[] values = {
                position.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_DELETE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Deleting position failed, no rows affected.");
            } else {
                position.setId(null);
            }
        } catch (SQLException e) {
            logger.error("Deleting position failed.", e);
        }
    }

    private Position map(ResultSet resultSet) throws SQLException {
        Position position = new Position();
        position.setId(resultSet.getLong("id"));
        position.setName(resultSet.getString("name"));
        position.setRoom(resultSet.getString("room"));
        position.setDescription(resultSet.getString("description"));
        return position;
    }
}

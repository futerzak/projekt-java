SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema signcourse
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema signcourse
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `signcourse` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `signcourse` ;

-- -----------------------------------------------------
-- Table `signcourse`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `signcourse`.`users` ;

CREATE TABLE IF NOT EXISTS `signcourse`.`users` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `album_number` INT(6) NULL,
  `name` VARCHAR(30) CHARACTER SET 'utf16' NOT NULL,
  `surname` VARCHAR(50) CHARACTER SET 'utf16' NOT NULL,
  `email` VARCHAR(50) CHARACTER SET 'utf16' NOT NULL UNIQUE KEY,
  `password` VARCHAR(255) NOT NULL,
  UNIQUE INDEX `nr_albumu` (`album_number` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf16;


-- -----------------------------------------------------
-- Table `signcourse`.`courses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `signcourse`.`courses` ;

CREATE TABLE IF NOT EXISTS `signcourse`.`courses` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) CHARACTER SET 'utf16' NOT NULL,
  `frequency` ENUM('tydzien','tygodnie','miesiac') CHARACTER SET 'utf16' NOT NULL,
  `user_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`, `user_id`),
  UNIQUE INDEX `id_przedmiotu` (`id` ASC),
  INDEX `fk_courses_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_courses_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `signcourse`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf16;


-- -----------------------------------------------------
-- Table `signcourse`.`positions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `signcourse`.`positions` ;

CREATE TABLE IF NOT EXISTS `signcourse`.`positions` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) CHARACTER SET 'utf16' NOT NULL,
  `room` VARCHAR(5) CHARACTER SET 'utf16' NOT NULL,
  `description` VARCHAR(70) CHARACTER SET 'utf16' NOT NULL,
  UNIQUE INDEX `id_stanowisko` (`id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf16;


-- -----------------------------------------------------
-- Table `signcourse`.`terms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `signcourse`.`terms` ;

CREATE TABLE IF NOT EXISTS `signcourse`.`terms` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(20) CHARACTER SET 'utf16' NOT NULL,
  `start_date` TIMESTAMP DEFAULT NOW(),
  `end_date` TIMESTAMP DEFAULT NOW(),
  `capacity` INT(11) NOT NULL,
  `size` INT(11) NOT NULL,
  `group` INT(11) NOT NULL,
  `courses_id` BIGINT NOT NULL,
  UNIQUE INDEX `id_terminu` (`id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_termin_courses1_idx` (`courses_id` ASC),
  CONSTRAINT `fk_termin_courses1`
    FOREIGN KEY (`courses_id`)
    REFERENCES `signcourse`.`courses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf16;


-- -----------------------------------------------------
-- Table `signcourse`.`roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `signcourse`.`roles` ;

CREATE TABLE IF NOT EXISTS `signcourse`.`roles` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nazwa_roli_UNIQUE` (`role` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `signcourse`.`users_terms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `signcourse`.`users_terms` ;

CREATE TABLE IF NOT EXISTS `signcourse`.`users_terms` (
  `user_id` BIGINT NOT NULL,
  `terms_id` BIGINT NOT NULL,
  `is_reserved` TINYINT(1) NULL,
  PRIMARY KEY (`user_id`, `terms_id`),
  INDEX `fk_users_has_terms_terms1_idx` (`terms_id` ASC),
  INDEX `fk_users_has_terms_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_users_has_terms_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `signcourse`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_terms_terms1`
    FOREIGN KEY (`terms_id`)
    REFERENCES `signcourse`.`terms` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf16;


-- -----------------------------------------------------
-- Table `signcourse`.`users_positions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `signcourse`.`users_positions` ;

CREATE TABLE IF NOT EXISTS `signcourse`.`users_positions` (
  `user_id` BIGINT NOT NULL,
  `positions_id` BIGINT NOT NULL,
  `date` DATETIME NULL,
  `is_reserved` TINYINT(1) NULL,
  PRIMARY KEY (`user_id`, `positions_id`),
  INDEX `fk_users_has_positions_positions1_idx` (`positions_id` ASC),
  INDEX `fk_users_has_positions_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_users_has_positions_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `signcourse`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_positions_positions1`
    FOREIGN KEY (`positions_id`)
    REFERENCES `signcourse`.`positions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf16;


-- -----------------------------------------------------
-- Table `signcourse`.`user_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `signcourse`.`user_roles` ;

CREATE TABLE IF NOT EXISTS `signcourse`.`user_roles` (
  `role_id` BIGINT NOT NULL,
  `user_id` BIGINT NOT NULL,
  PRIMARY KEY (`role_id`, `user_id`),
  INDEX `fk_roles_has_users_users1_idx` (`user_id` ASC),
  INDEX `fk_roles_has_user_roles1_idx` (`role_id` ASC),
  CONSTRAINT `fk_roles_has_user_roles1`
    FOREIGN KEY (`role_id`)
    REFERENCES `signcourse`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_roles_has_users_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `signcourse`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
